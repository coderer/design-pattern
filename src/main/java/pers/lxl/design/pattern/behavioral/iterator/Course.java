package pers.lxl.design.pattern.behavioral.iterator;

/**
 * Created by lxl.
 */
public class Course {
    private String name;

    public Course(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
