package pers.lxl.design.pattern.behavioral.iterator;

/**
 * Created by lxl.
 */
public interface CourseAggregate {

    void addCourse(Course course);

    void removeCourse(Course course);

    CourseIterator getCourseIterator();


}
