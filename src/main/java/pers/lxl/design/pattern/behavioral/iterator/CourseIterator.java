package pers.lxl.design.pattern.behavioral.iterator;

/**
 * Created by lxl.
 */
public interface CourseIterator {
    Course nextCourse();

    boolean isLastCourse();

}
