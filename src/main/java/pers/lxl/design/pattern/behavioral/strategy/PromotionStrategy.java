package pers.lxl.design.pattern.behavioral.strategy;

/**
 * Created by lxl
 */
public interface PromotionStrategy {
    void doPromotion();
}
