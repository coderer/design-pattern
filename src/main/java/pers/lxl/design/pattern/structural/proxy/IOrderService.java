package pers.lxl.design.pattern.structural.proxy;

/**
 * Created by lxl
 */
public interface IOrderService {
    int saveOrder(Order order);
}
