package pers.lxl.design.pattern.structural.flyweight;

/**
 * Created by lxl
 */
public interface Employee {
    void report();
}
