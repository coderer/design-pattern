package pers.lxl.design.pattern.structural.bridge;

/**
 * Created by lxl
 */
public interface Account {
    Account openAccount();

    void showAccountType();

}
