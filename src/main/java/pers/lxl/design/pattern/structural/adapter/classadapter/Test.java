package pers.lxl.design.pattern.structural.adapter.classadapter;

/**
 * Created by lxl
 */
public class Test {
    public static void main(String[] args) {
        Target target = new ConcreteTarget();
        target.request();

        Target adapterTarget = new Adapter();
        adapterTarget.request();


    }
}
