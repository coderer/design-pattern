package pers.lxl.design.pattern.structural.adapter.classadapter;

/**
 * Created by lxl
 */
public interface Target {
    void request();
}
