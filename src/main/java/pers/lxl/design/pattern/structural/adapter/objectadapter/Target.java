package pers.lxl.design.pattern.structural.adapter.objectadapter;

/**
 * Created by lxl
 */
public interface Target {
    void request();
}
