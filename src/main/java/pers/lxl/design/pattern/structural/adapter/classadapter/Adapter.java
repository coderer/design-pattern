package pers.lxl.design.pattern.structural.adapter.classadapter;

/**
 * Created by lxl
 */
public class Adapter extends Adaptee implements Target {
    @Override
    public void request() {
        //...
        super.adapteeRequest();
        //...
    }
}
