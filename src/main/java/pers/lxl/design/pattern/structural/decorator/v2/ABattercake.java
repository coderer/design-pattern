package pers.lxl.design.pattern.structural.decorator.v2;

/**
 * Created by lxl
 */
public abstract class ABattercake {
    protected abstract String getDesc();

    protected abstract int cost();

}
