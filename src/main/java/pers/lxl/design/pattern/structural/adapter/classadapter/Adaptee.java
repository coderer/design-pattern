package pers.lxl.design.pattern.structural.adapter.classadapter;

/**
 * Created by lxl
 */
public class Adaptee {
    public void adapteeRequest() {
        System.out.println("被适配者的方法");
    }

}
