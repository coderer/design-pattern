package pers.lxl.design.pattern.structural.proxy;

/**
 * Created by lxl
 */
public interface IOrderDao {
    int insert(Order order);

}
