package pers.lxl.design.pattern.structural.adapter.objectadapter;

/**
 * Created by lxl
 */
public class Adapter implements Target {
    private Adaptee adaptee = new Adaptee();

    @Override
    public void request() {
        //...
        adaptee.adapteeRequest();
        //...
    }
}
