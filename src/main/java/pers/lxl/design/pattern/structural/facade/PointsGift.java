package pers.lxl.design.pattern.structural.facade;

/**
 * Created by lxl
 */
public class PointsGift {
    private String name;

    public PointsGift(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
