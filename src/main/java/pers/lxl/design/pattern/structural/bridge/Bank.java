package pers.lxl.design.pattern.structural.bridge;

/**
 * Created by lxl
 */
public abstract class Bank {
    protected Account account;

    public Bank(Account account) {
        this.account = account;
    }

    abstract Account openAccount();

}
