package pers.lxl.design.pattern.structural.proxy.dynamicproxy;

import pers.lxl.design.pattern.structural.proxy.IOrderService;
import pers.lxl.design.pattern.structural.proxy.Order;
import pers.lxl.design.pattern.structural.proxy.OrderServiceImpl;

/**
 * Created by lxl
 */
public class Test {
    public static void main(String[] args) {
        Order order = new Order();
//        order.setUserId(2);
        order.setUserId(1);
        IOrderService orderServiceDynamicProxy = (IOrderService) new OrderServiceDynamicProxy(new OrderServiceImpl()).bind();

        orderServiceDynamicProxy.saveOrder(order);
    }
}
