package pers.lxl.design.pattern.structural.decorator.v1;

/**
 * Created by lxl
 */
public class Battercake {
    protected String getDesc() {
        return "煎饼";
    }

    protected int cost() {
        return 8;
    }

}
