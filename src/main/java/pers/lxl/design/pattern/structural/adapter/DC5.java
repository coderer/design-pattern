package pers.lxl.design.pattern.structural.adapter;

/**
 * Created by lxl
 */
public interface DC5 {
    int outputDC5V();
}
