package pers.lxl.design.pattern.creational.abstractfactory;


/**
 * Created by lxl
 */
public interface CourseFactory {
    Video getVideo();

    Article getArticle();

}
