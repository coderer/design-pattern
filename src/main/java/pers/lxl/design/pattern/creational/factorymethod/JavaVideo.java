package pers.lxl.design.pattern.creational.factorymethod;

/**
 * Created by lxl
 */
public class JavaVideo extends Video {
    @Override
    public void produce() {
        System.out.println("录制Java课程视频");
    }
}
