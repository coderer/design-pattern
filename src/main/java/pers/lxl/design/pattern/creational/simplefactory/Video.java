package pers.lxl.design.pattern.creational.simplefactory;

/**
 * Created by lxl
 */
public abstract class Video {
    public abstract void produce();

}
