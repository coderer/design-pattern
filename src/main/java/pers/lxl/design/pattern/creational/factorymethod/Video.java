package pers.lxl.design.pattern.creational.factorymethod;

/**
 * Created by lxl
 */
public abstract class Video {
    public abstract void produce();

}
