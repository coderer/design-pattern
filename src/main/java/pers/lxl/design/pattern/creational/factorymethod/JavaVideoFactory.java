package pers.lxl.design.pattern.creational.factorymethod;

/**
 * Created by lxl
 */
public class JavaVideoFactory extends VideoFactory {
    @Override
    public Video getVideo() {
        return new JavaVideo();
    }
}
