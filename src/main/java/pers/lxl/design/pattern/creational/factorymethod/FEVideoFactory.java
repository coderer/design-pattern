package pers.lxl.design.pattern.creational.factorymethod;

/**
 * Created by lxl
 */
public class FEVideoFactory extends VideoFactory {
    @Override
    public Video getVideo() {
        return new FEVideo();
    }
}
