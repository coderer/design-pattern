package pers.lxl.design.pattern.creational.abstractfactory;

/**
 * Created by lxl
 */
public class PythonVideo extends Video {
    @Override
    public void produce() {
        System.out.println("录制Python课程视频");
    }
}
