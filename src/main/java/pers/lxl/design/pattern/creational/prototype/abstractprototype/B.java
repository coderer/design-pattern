package pers.lxl.design.pattern.creational.prototype.abstractprototype;

/**
 * Created by lxl
 */
public class B extends A {
    public static void main(String[] args) throws CloneNotSupportedException {
        B b = new B();
        b.clone();
    }
}
