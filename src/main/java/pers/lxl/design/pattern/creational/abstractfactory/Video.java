package pers.lxl.design.pattern.creational.abstractfactory;

/**
 * Created by lxl
 */
public abstract class Video {
    public abstract void produce();

}
