package pers.lxl.design.pattern.creational.factorymethod;

/**
 * Created by lxl
 */
public class PythonVideoFactory extends VideoFactory {
    @Override
    public Video getVideo() {
        return new PythonVideo();
    }
}
