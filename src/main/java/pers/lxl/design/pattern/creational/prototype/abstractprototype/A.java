package pers.lxl.design.pattern.creational.prototype.abstractprototype;

/**
 * Created by lxl
 */
public abstract class A implements Cloneable {
    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
