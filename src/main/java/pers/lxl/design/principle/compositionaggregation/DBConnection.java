package pers.lxl.design.principle.compositionaggregation;

/**
 * Created by lxl
 */
public abstract class DBConnection {
    //    public String getConnection(){
//        return "MySQL数据库连接";
//    }
    public abstract String getConnection();
}
