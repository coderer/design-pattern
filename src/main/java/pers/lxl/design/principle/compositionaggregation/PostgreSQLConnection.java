package pers.lxl.design.principle.compositionaggregation;

/**
 * Created by lxl
 */
public class PostgreSQLConnection extends DBConnection {
    @Override
    public String getConnection() {
        return "PostgreSQL数据库连接";
    }
}
