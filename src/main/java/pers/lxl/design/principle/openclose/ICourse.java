package pers.lxl.design.principle.openclose;

/**
 * Created by lxl
 */
public interface ICourse {
    Integer getId();

    String getName();

    Double getPrice();


}
