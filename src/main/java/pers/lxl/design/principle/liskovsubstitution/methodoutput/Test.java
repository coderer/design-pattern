package pers.lxl.design.principle.liskovsubstitution.methodoutput;

/**
 * Created by lxl
 */
public class Test {
    public static void main(String[] args) {
        Child child = new Child();
        System.out.println(child.method());

    }
}
