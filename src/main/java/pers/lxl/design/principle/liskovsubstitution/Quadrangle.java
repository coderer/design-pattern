package pers.lxl.design.principle.liskovsubstitution;

/**
 * Created by lxl
 */
public interface Quadrangle {
    long getWidth();

    long getLength();

}
