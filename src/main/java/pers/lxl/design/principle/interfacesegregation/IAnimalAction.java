package pers.lxl.design.principle.interfacesegregation;

/**
 * Created by lxl
 */
public interface IAnimalAction {
    void eat();

    void fly();

    void swim();

}
