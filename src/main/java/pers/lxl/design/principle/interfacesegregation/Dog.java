package pers.lxl.design.principle.interfacesegregation;

/**
 * Created by lxl
 */
public class Dog implements ISwimAnimalAction, IEatAnimalAction {

    @Override
    public void eat() {

    }

    @Override
    public void swim() {

    }
}
