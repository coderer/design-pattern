package pers.lxl.design.principle.interfacesegregation;

/**
 * Created by lxl
 */
public interface IFlyAnimalAction {
    void fly();
}
