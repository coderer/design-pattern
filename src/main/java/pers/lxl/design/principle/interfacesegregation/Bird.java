package pers.lxl.design.principle.interfacesegregation;

/**
 * Created by lxl
 */
public class Bird implements IAnimalAction {
    @Override
    public void eat() {

    }

    @Override
    public void fly() {

    }

    @Override
    public void swim() {

    }
}
