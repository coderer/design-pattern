package pers.lxl.design.principle.interfacesegregation;

/**
 * Created by lxl
 */
public interface IEatAnimalAction {
    void eat();
}
