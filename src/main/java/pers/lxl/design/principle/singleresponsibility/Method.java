package pers.lxl.design.principle.singleresponsibility;

/**
 * Created by lxl
 */
public class Method {
    private void updateUserInfo(String userName, String address) {
        userName = "lxl";
        address = "beijing";
    }

    private void updateUserInfo(String userName, String... properties) {
        userName = "lxl";
//        address = "beijing";
    }

    private void updateUsername(String userName) {
        userName = "lxl";
    }

    private void updateUserAddress(String address) {
        address = "beijing";
    }

    private void updateUserInfo(String userName, String address, boolean bool) {
        if (bool) {
            //todo something1
        } else {
            //todo something2
        }


        userName = "lxl";
        address = "beijing";
    }


}
