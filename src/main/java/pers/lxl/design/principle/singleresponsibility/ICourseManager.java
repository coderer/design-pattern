package pers.lxl.design.principle.singleresponsibility;

/**
 * Created by lxl
 */
public interface ICourseManager {
    void studyCourse();

    void refundCourse();
}
