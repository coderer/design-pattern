package pers.lxl.design.principle.singleresponsibility;

/**
 * Created by lxl
 */
public interface ICourse {
    String getCourseName();

    byte[] getCourseVideo();

    void studyCourse();

    void refundCourse();

}
