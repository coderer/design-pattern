package pers.lxl.design.principle.singleresponsibility;

/**
 * Created by lxl
 */
public interface ICourseContent {
    String getCourseName();

    byte[] getCourseVideo();
}
