package pers.lxl.design.principle.singleresponsibility;

/**
 * Created by lxl
 */
public class WalkBird {
    public void mainMoveMode(String birdName) {
        System.out.println(birdName + "用脚走");
    }
}
