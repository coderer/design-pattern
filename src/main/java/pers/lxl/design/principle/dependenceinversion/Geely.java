package pers.lxl.design.principle.dependenceinversion;

/**
 * Created by lxl
 */
public class lxl {

    private ICourse iCourse;

    public void setiCourse(ICourse iCourse) {
        this.iCourse = iCourse;
    }

    public void studyImoocCourse() {
        iCourse.studyCourse();
    }


}
