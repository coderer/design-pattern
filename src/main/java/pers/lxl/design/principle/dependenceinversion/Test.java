package pers.lxl.design.principle.dependenceinversion;

/**
 * Created by lxl
 */
public class Test {

    //v1
//    public static void main(String[] args) {
//        lxl lxl = new lxl();
//        lxl.studyJavaCourse();
//        lxl.studyFECourse();
//    }

    //v2
//    public static void main(String[] args) {
//        lxl lxl = new lxl();
//        lxl.studyImoocCourse(new JavaCourse());
//        lxl.studyImoocCourse(new FECourse());
//        lxl.studyImoocCourse(new PythonCourse());
//    }

    //v3
//    public static void main(String[] args) {
//        lxl lxl = new lxl(new JavaCourse());
//        lxl.studyImoocCourse();
//    }
    public static void main(String[] args) {
        lxl lxl = new lxl();
        lxl.setiCourse(new JavaCourse());
        lxl.studyImoocCourse();

        lxl.setiCourse(new FECourse());
        lxl.studyImoocCourse();

    }


}
