package pers.lxl.design.principle.dependenceinversion;

/**
 * Created by lxl
 */
public class PythonCourse implements ICourse {
    @Override
    public void studyCourse() {
        System.out.println("lxl在学习Python课程");
    }
}
