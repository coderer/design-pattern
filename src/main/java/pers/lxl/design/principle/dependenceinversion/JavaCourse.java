package pers.lxl.design.principle.dependenceinversion;

/**
 * Created by lxl
 */
public class JavaCourse implements ICourse {

    @Override
    public void studyCourse() {
        System.out.println("lxl在学习Java课程");
    }
}
