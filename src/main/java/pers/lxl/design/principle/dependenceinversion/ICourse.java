package pers.lxl.design.principle.dependenceinversion;

/**
 * Created by lxl
 */
public interface ICourse {
    void studyCourse();
}
