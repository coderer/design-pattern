package pers.lxl.design.thread;

/**
 * 父亲一个人数一亿粒米
 */
public class FatherCounter implements Counter {
    @Override
    public long count(double[] riceArray) {
        long result = 0L;
        for (double rice : riceArray) {
            result += 1;
            // 当大于等于1亿粒米时退出
//            if (result >= 1e8) {
//                break;
//            }
        }
        return result;
    }
}
