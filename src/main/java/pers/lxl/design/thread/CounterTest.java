package pers.lxl.design.thread;

/**
 * @describe: 测试计算一亿消耗的时间
 * @author: lxl
 * @date: 2018/10/21 19:11
 * @version: v1.0
 */
public class CounterTest {

    public static void main(String[] args) {

        long riceArrayLength = (long) 40000000;
        double[] riceArray = createArray(riceArrayLength);

        long startTime = System.currentTimeMillis();

//        // 测试单线程
//        FatherCounter fatherCounter = new FatherCounter();
//        long result = fatherCounter.count(riceArray);

//        // 测试多线程
//        FamilyCounter familyCounter = new FamilyCounter(4);
//        long count = familyCounter.count(riceArray);

//        // 测试fork-join模式
        TogetherCounter togetherCounter = new TogetherCounter(4);
        long count1 = togetherCounter.count(riceArray);
        System.out.println(count1);


        long endTime = System.currentTimeMillis();

        System.out.println("耗时：" + (endTime - startTime) + "ms");
    }


    private static double[] createArray(long length) {
        double[] array = new double[(int) length];
        for (int i = 0; i < length; i++) {
            array[i] = 1;
        }
        return array;
    }
}

