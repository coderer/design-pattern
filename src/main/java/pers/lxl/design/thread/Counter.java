package pers.lxl.design.thread;

/**
 * @describe: 计算接口
 * @author: lxl
 * @date: 2018年10月21日18:54:24
 */
public interface Counter {
    /**
     * 计算
     *
     * @param riceArray 要计算的数组(计算一亿粒米)
     * @return 计算结果
     */
    long count(double[] riceArray);
}
